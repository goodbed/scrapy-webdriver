""":mod:`scrapy_webdriver` -- A module with webdriver helpers for scrapy"""

from scrapy_webdriver import metadata

__version__ = metadata.version
__author__ = metadata.authors[0]
__license__ = metadata.license
__copyright__ = metadata.copyright
